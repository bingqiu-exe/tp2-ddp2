import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)


public class SheepArrays extends Sheep
{
    private int sizer;
    private GreenfootImage image2 = new GreenfootImage("sheep.png");
    
    private static final int EAST = 0;
    private static final int WEST = 1;
    private static final int NORTH = 2;
    private static final int SOUTH = 3;

    private int direction;
    
    //private int[][] theMaze;
    AutoWorld counter;
    private int pointsEaten = 0;
    private int foodEaten = 0;
    AutoWorld maze;
    
    public SheepArrays() 
    {
        
    }
    
    public void act()
    {
        imgSizer();
        eatPoint();
        eatFood();
        checkForSnakeCollision();
        checkForPoisonCollision();
        moveByMap();
        /**encounterWall();
        walkOnPath();*/
    }
    
    public void imgSizer()
    {
        GreenfootImage img = new GreenfootImage(image2);
        img.scale((img.getWidth()/2), (img.getHeight()/2));
        setImage(img);
    }
    
    public boolean canSee(Class clss)
    {
        Actor actor = getOneObjectAtOffset(0, 0, clss);
        return actor != null;        
    }
    
    public void eat(Class clss)
    {
        Actor actor = getOneObjectAtOffset(0, 0, clss);
        if(actor != null) 
        {
            getWorld().removeObject(actor);
        }
    }
    
    public void eatPoint()
    {
        if (canSee(Point.class))
        {
            eat(Point.class);
            pointsEaten = pointsEaten + 1;
            counter.life = counter.life + 1;
            counter.score = counter.life * 50;
        }
    }
    
    public void eatFood()
    {
        if (canSee(Food.class))
        {
            eat(Food.class);
            pointsEaten = pointsEaten + 1;
            counter.health = counter.health + 5;
            counter.score = counter.health * 20;
        }
    }
    
    public void checkForSnakeCollision()
    {
        if (canSee(Snake.class))
        {
            counter.life -= 1;
            if (counter.life < 0)
            {
                Greenfoot.setWorld(new Gameover());
                counter.life = 0;
            }
        }
    }
    
    public void checkForPoisonCollision()
    {
        if (canSee(Poison.class))
        {
            counter.health -= 5;
            if (counter.health < -5)
            {
            Greenfoot.setWorld(new Gameover());
                counter.life = 0;
            }
        }
    }
    
    public void moveByMap()
    {
        if (checkMap(getX()+1, getY()))
        {
            setMap(getX(), getY(), 2);
            setLocation(getX()+1, getY());
        }
        else if (checkMap(getX()-1, getY()))
        {
            setMap(getX(), getY(), 2);
            setLocation(getX()-1, getY());
        }
        else if (checkMap(getX(), getY()+1))
        {
            setMap(getX(), getY(), 2);
            setLocation(getX(), getY()+1);
        }
        else if (checkMap(getX(), getY()-1))
        {
            setMap(getX(), getY(), 2);
            setLocation(getX(), getY()-1);
        }
    }
    
    public boolean checkMap(int x, int y){
        World myWorld = getWorld();
        if (x >= 0 && x <= maze.getMaze().length && y>= 0 && y <= maze.getMaze().length)
        {
            if (maze.getMaze()[x][y] == 1) 
            {
                return true;
            }
            else 
            {
                return false;
            }
        }
        else {
            return false;
        }
    }
    
    private void setMap(int x, int y, int code)
    {
        maze.getMaze()[x][y]=2;
    }
    
    /**public void encounterWall()
    {
        int xOffset = 0;
        int yOffset = 0;
        int futureX = getX() + xOffset;
        int futureY = getY() + yOffset;
        
        Actor wall = getOneObjectAtOffset(xOffset, yOffset, Wall.class);
        
        if (wall == null)
        {
            setLocation(futureX, futureY);
        }
        else
        {
            counter.score = counter.score - 50;
        }
    }*/
    
    /**public boolean canMove()
    {
        World myWorld = getWorld();
        int x = getX();
        int y = getY();
        switch(direction)
        {
            case SOUTH :
                y++;
                break;
            case EAST :
                x++;
                break;
            case NORTH :
                y--;
                break;
            case WEST :
                x--;
                break;
        }
        // test for outside border
        if (x >= myWorld.getWidth() || y >= myWorld.getHeight()) {
            return false;
        }
        else if (x < 0 || y < 0) {
            return false;
        }
        return true;
    }
    
    public void move()
    {
        if (!canMove()) {
            return;  // doing nothing
        }
        switch(direction) {
            case SOUTH :
                setLocation(getX(), getY() + 1);
                break;
            case EAST :
                setLocation(getX() + 1, getY());
                break;
            case NORTH :
                setLocation(getX(), getY() - 1);
                break;
            case WEST :
                setLocation(getX() - 1, getY());
                break;
        }
    }
    
    public void walkOnPath()
    {
        int x = getX();
        int y = getY();
        int xOffset = 0;
        int yOffset = 0;
        
        if (canSee(Point.class))
        {
            
        }
    }*/
    
    /**public boolean checkPath(int x, int y)
    {
        
    }*/
}


