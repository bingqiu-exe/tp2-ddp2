import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Point here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Point extends Actor
{
    /**
     * Act - do whatever the Point wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    private GreenfootImage image = new GreenfootImage("blue-draught.png");
    private int sizer;
    
    public void act()
    {
        imgSizer();
    }
    
    public void imgSizer()
    {
        GreenfootImage img = new GreenfootImage(image);
        img.scale((img.getWidth()/4), (img.getHeight()/4));
        setImage(img);
    }
}
