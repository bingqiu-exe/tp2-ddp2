import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)


public class AutoWorld extends World
{
    public static int score = 0;
    public static int life = 0;
    public static int health = 0;
    public static int[][] theMaze;    
    public AutoWorld()
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(27,20, 25, true); 
        Sheep s1 = new SheepArrays();
        addObject(s1, 0, 0);
        prepareMaze();
        life = 0;
        health = 0;
        score = 0;
    }
    
    public void prepareMaze(){
        MazeGenerator mazeGenerator = new MazeGenerator(20);
        mazeGenerator.generateMaze();
        int[][] theMaze = mazeGenerator.getMaze(); //2D array yang menyimpan informasi Maze
        
        // anda dapat mengolah 2D Array menjadi Maze pada screen disini
        for (int row = 0; row < theMaze.length; row++)
        {
            for (int col = 0; col < theMaze[0].length; col++)
            {
                if (theMaze[row][col] == 0) 
                {
                    addObject(new Wall(), col, row);
                }
                else
                {
                    addObject(new Point(), col, row);
                    if (Greenfoot.getRandomNumber(100) < 3)
                    {
                        addObject(new Food(), col, row);
                    }
                    if (Greenfoot.getRandomNumber(200) < 2)
                    {
                        addObject(new Snake(), col, row);
                    }
                    if (Greenfoot.getRandomNumber(200) < 3)
                    {
                        addObject(new Poison(), col, row);
                    }
                }
            }
        }
        //Tembok pembatas
        for (int i =0; i<20; i++)
        {
            addObject(new Wall(),20,i);
        }
    }
    
    public int[][] getMaze()
    {
        return theMaze;
    }
    
    public void act()
    {
        showText("Life: "+ life, 23, 2);
        if (life < 0)
        {
            Greenfoot.setWorld(new Gameover());
        }
        showText("Health: "+ health, 23, 4);
        showText("Highest", 23, 15);
        showText("score: "+ score, 23, 16);
        /**if (score >= 3)
        {
            Greenfoot.setWorld(new Gameover());
        }*/
    }
}
