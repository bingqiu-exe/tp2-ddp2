import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class ButtonMainLagi here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class ButtonMainLagi extends Actor
{
    /**
     * Act - do whatever the ButtonMainLagi wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public ButtonMainLagi()
    {
        GreenfootImage image = new GreenfootImage("Main lagi", 40, Color.GREEN, null);
        setImage(image);
    }
}
