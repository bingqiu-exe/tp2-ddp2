import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Gameover here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Gameover extends World
{
    ButtonMainLagi button1 = new ButtonMainLagi();
    ButtonKeluar button2 = new ButtonKeluar();
    ManualWorld counter;
    public Gameover()
    {    
        super(27,20, 25, true);
        titleOver();
        prepare();
    }
    
    public void titleOver()
    {
        GreenfootImage image1 = new GreenfootImage("GAME OVER", 60, Color.RED, null);
        getBackground().drawImage(image1, 330-image1.getWidth()/2, 100-image1.getHeight()/2);
        prepare();
    }
    
    private void prepare() 
    {
        addObject(button1, 7, 15);
        addObject(button2, 20, 15);
    }
    
    public void act()
    {
        if (counter.score > counter.previousscore) 
        {
            counter.previousscore = counter.score;
        }
        GreenfootImage image2 = new GreenfootImage("skor anda: " + counter.score, 30, Color.BLACK, null);
        getBackground().drawImage(image2, 300-image2.getWidth()/2, 200-image2.getHeight()/2);
        GreenfootImage image3 = new GreenfootImage("skor tertinggi: " + counter.previousscore, 30, Color.BLACK, null);
        getBackground().drawImage(image3, 300-image3.getWidth()/2, 250-image3.getHeight()/2);
        if (Greenfoot.mouseClicked(button1)) 
        {
            Greenfoot.start();
            counter.life = 0;
            counter.health = 0;
            Greenfoot.setWorld(new ManualWorld());
        }
        else if (Greenfoot.mouseClicked(button2)) 
        {
            Greenfoot.start();
            counter.life = 0;
            counter.health = 0;
            Greenfoot.setWorld(new Homescreen());
        }
    }
}
