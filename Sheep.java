import greenfoot.*;


public class Sheep extends Actor
{
    /**
     * Act - do whatever the Sheep wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public static final double WALKING_SPEED = 5.0;
    public static final int EAST = 0;
    public static final int WEST = 1;
    public static final int NORTH = 2;
    public static final int SOUTH = 3;

    private int direction;
    private int sizer;
    private GreenfootImage image2 = new GreenfootImage("sheep.png");
    ManualWorld counter;
    private int pointsEaten = 0;
    private int foodEaten = 0;
    
    public void act()
    {
        checkKeyPress();
        imgSizer();
        eatPoint();
        eatFood();
        checkForSnakeCollision();
        checkForPoisonCollision();
    }
    
    public void imgSizer()
    {
        GreenfootImage img = new GreenfootImage(image2);
        img.scale((img.getWidth()/2), (img.getHeight()/2));
        setImage(img);
    }
    
    public void setDirection(int direction)
    {
        if ((direction >= 0) && (direction <= 3))
        {
            this.direction = direction;
        }
        switch(direction) {
            case SOUTH :
                setRotation(90);
                break;
            case EAST :
                setRotation(0);
                break;
            case NORTH :
                setRotation(270);
                break;
            case WEST :
                setRotation(180);
                break;
            default :
                break;
        }
    }
    
    public boolean canSee(Class clss)
    {
        Actor actor = getOneObjectAtOffset(0, 0, clss);
        return actor != null;        
    }
    
    public void eat(Class clss)
    {
        Actor actor = getOneObjectAtOffset(0, 0, clss);
        if(actor != null) 
        {
            getWorld().removeObject(actor);
        }
    }
    
    public void eatPoint()
    {
        if (canSee(Point.class))
        {
            eat(Point.class);
            pointsEaten = pointsEaten + 1;
            counter.life = counter.life + 1;
            counter.score = counter.life * 50;
        }
        /**else if (canSee(Point.class))
        {
            eat(Point.class);
            pointsEaten = pointsEaten + 1;
            counter.score = counter.life * 5
        }*/
    }
    
    public void eatFood()
    {
        if (canSee(Food.class))
        {
            eat(Food.class);
            pointsEaten = pointsEaten + 1;
            counter.health = counter.health + 5;
            counter.score = counter.health * 20;
        }
    }
    
    public void checkForSnakeCollision()
    {
        if (canSee(Snake.class))
        {
            counter.life -= 1;
            if (counter.life < 0)
            {
                Greenfoot.setWorld(new Gameover());
                counter.life = 0;
            }
        }
    }
    
    public void checkForPoisonCollision()
    {
        if (canSee(Poison.class))
        {
            counter.health -= 5;
            if (counter.health < -5)
            {
            Greenfoot.setWorld(new Gameover());
                counter.life = 0;
            }
        }
    }
    
    public void checkKeyPress()
    {
        int speed = 1;
        int xOffset = 0;
        int yOffset = 0;
        
        if (Greenfoot.isKeyDown("up"))
        {
            setDirection(NORTH);
            yOffset = -speed;
        }
        if (Greenfoot.isKeyDown("left"))
        {
            setDirection(WEST);
            xOffset = -speed;
        }
        if (Greenfoot.isKeyDown("right"))
        {
            setDirection(EAST);
            xOffset = speed;
        }
        if (Greenfoot.isKeyDown("down"))
        {
            setDirection(SOUTH);
            yOffset = speed;
        }
        
        int futureX = getX() + xOffset;
        int futureY = getY() + yOffset;
        
        Actor wall = getOneObjectAtOffset(xOffset, yOffset, Wall.class);
        
        if (wall == null)
        {
            setLocation(futureX, futureY);
        }
        else
        {
            counter.score = counter.score - 50;
        }
    }
}
