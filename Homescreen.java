import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

public class Homescreen extends World
{
    ButtonManualWorld button1 = new ButtonManualWorld();
    ButtonAutoWorld button2 = new ButtonAutoWorld();
    
    public Homescreen()
    {    
        super(600, 400, 1);
        titleScreen();
        prepare();
    }
    
    public void titleScreen()
    {
        GreenfootImage image = new GreenfootImage("Sheep In A Maze", 60, Color.BLACK, null);
        getBackground().drawImage(image, 300-image.getWidth()/2, 100-image.getHeight()/2);
        prepare();
    }
    
    private void prepare() 
    {
        addObject(button1, 180, 300);
        addObject(button2, 430, 300);
    }
    
    public void act()
    {
        if (Greenfoot.mouseClicked(button1)) 
        {
            Greenfoot.start();
            Greenfoot.setWorld(new ManualWorld());
        }
        else if (Greenfoot.mouseClicked(button2)) 
        {
            Greenfoot.start();
            Greenfoot.setWorld(new AutoWorld());
        }
    }
}
