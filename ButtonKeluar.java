import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class ButtonKeluar here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class ButtonKeluar extends Actor
{
    /**
     * Act - do whatever the ButtonKeluar wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public ButtonKeluar()
    {
        GreenfootImage image2 = new GreenfootImage("Keluar", 40, Color.BLUE, null);
        setImage(image2);
    }
}
