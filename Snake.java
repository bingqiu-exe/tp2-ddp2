import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Snake here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Snake extends Actor
{
    private int sizer;
    private GreenfootImage image4 = new GreenfootImage("snake2.png");
    private int roamRange = 5;
    private int roamCounter = 0;
    private int currentDirection;
    
    public Snake()
    {
        currentDirection = Greenfoot.getRandomNumber(4);
    }
    
    public void act()
    {
        imgSizer();
        roamMaze();
    }
    
    public void imgSizer()
    {
        GreenfootImage img = new GreenfootImage(image4);
        img.scale((img.getWidth()/2), (img.getHeight()/2));
        setImage(img);
    }
    
    public void roamMaze()
    {
        if (roamCounter <= 0) 
        {
            int newDirection = currentDirection + Greenfoot.getRandomNumber(roamRange * 2 + 1) - roamRange;
            newDirection = (newDirection + 4) % 4;
            
            int newX = getX();
            int newY = getY();
            
            switch (newDirection)
            {
                case 0: // Up
                    newY--;
                    break;
                case 1: // Right
                    newX++;
                    break;
                case 2: // Down
                    newY++;
                    break;
                case 3: // Left
                    newX--;
                    break;
            }
            if (newX >= 0 && newX < getWorld().getWidth() && newY >= 0 && newY < getWorld().getHeight() && getWorld().getObjectsAt(newX, newY, Wall.class).isEmpty())
            {
                setLocation(newX, newY);
                currentDirection = newDirection;
            }
            roamCounter = 3;
        }
        else
        {
            roamCounter--;
        }
    }
    
    /**public void checkForWalls()
    {
        int xOffset = 0;
        int yOffset = 0;
        int futureX = getX() + xOffset;
        int futureY = getY() + yOffset;
        
        Actor wall = getOneObjectAtOffset(xOffset, yOffset, Wall.class);
        
        if (wall == null)
        {
            setLocation(futureX, futureY);
        }
    }*/
}