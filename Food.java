import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Food here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Food extends Actor
{
    private int sizer;
    private GreenfootImage image3 = new GreenfootImage("flower2.png");
    public void act()
    {
        imgSizer();
    }
    
    public void imgSizer()
    {
        GreenfootImage img = new GreenfootImage(image3);
        img.scale((img.getWidth()/2), (img.getHeight()/2));
        setImage(img);
    }
}
